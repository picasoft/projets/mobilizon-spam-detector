SELECT DISTINCT events.id, events.organizer_actor_id, users.email FROM events
INNER JOIN actors on actors.id = events.organizer_actor_id
INNER JOIN users on users.id = actors.user_id
WHERE
    events.LOCAL = TRUE
    AND events.draft = FALSE
    AND events.visibility = 'public'
    AND events.inserted_at > '__MOBILIZON_SPAM_LAST_CHECK__'
;
