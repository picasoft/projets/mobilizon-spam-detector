#!/bin/bash
# calls script to create Reports about spam
###########################################

# 0. get a token
TOKEN=$(bash ./get_token.sh)

# 1. export for second script
export BEARER=${TOKEN}

# 2. execute SQL query and call API
bash ./report_spam.sh
