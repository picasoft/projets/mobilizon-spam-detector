SELECT id, organizer_actor_id, url FROM events
WHERE
    LOCAL = TRUE
    AND draft = FALSE
    AND visibility = 'public'
    AND (title ILIKE '% __PATTERN__ %' OR description ILIKE '% __PATTERN__ %')
    AND inserted_at > '__MOBILIZON_SPAM_LAST_CHECK__'
;
