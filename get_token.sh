#!/bin/bash
# query API to get a token
# requires:
#   MOBILIZON_EP: URL of mobilizon API
#   MOBILIZON_API_USER_EMAIL: email of the user
#   MOBILIZON_API_USER_PASSWORD: password of the user
# output:
#   string: value of the token
########################################################

# variables definition if not present in environment
[ -z "${MOBILIZON_EP}" ] && MOBILIZON_EP="https://mobilizon.picasoft.net/api"
[ -z "${MOBILIZON_API_USER_EMAIL}" ] && MOBILIZON_API_USER_EMAIL="picasoft+moderation@assos.utc.fr"
[ -z "${MOBILIZON_API_USER_PASSWORD}" ] && echo "USER PASSWORD is missing" && exit 2


gql='mutation Login($email: String!, $password: String!) {
    login(
        email: $email
        password: $password
    ) {
        accessToken
        refreshToken
    }
}'

printf -v vars '{ "email" : "%s", "password" : "%s" }' "${MOBILIZON_API_USER_EMAIL}" "${MOBILIZON_API_USER_PASSWORD}"


TOKEN=$(curl -fsS \
    -H 'accept: application/json' \
    -X 'POST' \
    -F "query=${gql}" \
    -F "variables=${vars}" \
    -F "operationName=Login" \
    "${MOBILIZON_EP}" | jq -r .data.login.accessToken)

[ -z "${TOKEN}" ] && echo "ERROR while getting TOKEN" && exit 1

echo ${TOKEN}
