#!/bin/bash
# query PG database to get event_id, user_id and url
# filter on SPAM keywords from title and description
# filter on EMAILS DOMAINS from users
# requires:
#	BEARER: JWT
#   MOBILIZON_SPAM_EXPORT_FOLDER: folder name to export CSV to
#   MOBILIZON_SPAM_FILE_KEYWORDS: path to a txt file with keywords
#   MOBILIZON_SPAM_FILE_EMAILS_DOMAINS: path to a txt file with emails domains
#   MOBILIZON_SPAM_LAST_CHECK: path to a txt file with last_check format ``date "+%Y-%m-%d %H:%M:%S%:::z"``
#   MOBILIZON_EP: URL of mobilizon API
########################################################

# variables definition if not present in environment
[ -z "${BEARER}" ] && echo "BEARER is required to call Mobilizon API" && exit 5
[ -z "${MOBILIZON_DATABASE_DBNAME}" ] && echo "MOBILIZON_DATABASE_DBNAME is required connect DataBase" && exit 6
[ -z "${MOBILIZON_DATABASE_HOST}" ] && echo "MOBILIZON_DATABASE_HOST is required connect DataBase" && exit 7
[ -z "${MOBILIZON_DATABASE_PASSWORD}" ] && echo "MOBILIZON_DATABASE_PASSWORD is required connect DataBase" && exit 8
[ -z "${MOBILIZON_DATABASE_USERNAME}" ] && echo "MOBILIZON_DATABASE_USERNAME is required connect DataBase" && exit 9
[ -z "${MOBILIZON_SPAM_EXPORT_FOLDER}" ] && MOBILIZON_SPAM_EXPORT_FOLDER="spam_export"
[ -z "${MOBILIZON_SPAM_FILE_KEYWORDS}" ] && MOBILIZON_SPAM_FILE_KEYWORDS="keywords_spam.txt"
[ -z "${MOBILIZON_SPAM_FILE_EMAILS_DOMAINS}" ] && MOBILIZON_SPAM_FILE_EMAILS_DOMAINS="emails_domains_spam.txt"
[ -z "${MOBILIZON_SPAM_LAST_CHECK}" ] && MOBILIZON_SPAM_LAST_CHECK="last_check.txt"
[ -z "${MOBILIZON_EP}" ] && MOBILIZON_EP="https://mobilizon.picasoft.net/api"
[ -z "${MOBILIZON_QUERY_TPL_KEYWORDS}" ] && MOBILIZON_QUERY_TPL_KEYWORDS="query.keyword.tpl.sql"
[ -z "${MOBILIZON_QUERY_TPL_EMAILS_DOMAINS}" ] && MOBILIZON_QUERY_TPL_EMAILS_DOMAINS="query.email.tpl.sql"


# check rights
[ ! -f ${MOBILIZON_SPAM_FILE_KEYWORDS} ] && echo "MOBILIZON_SPAM_FILE_KEYWORDS (${MOBILIZON_SPAM_FILE_KEYWORDS}) is missing" && exit 2
[ ! -r ${MOBILIZON_SPAM_FILE_KEYWORDS} ] && echo "MOBILIZON_SPAM_FILE_KEYWORDS (${MOBILIZON_SPAM_FILE_KEYWORDS}) is not readable" && exit 3
[ ! -d ${MOBILIZON_SPAM_EXPORT_FOLDER} ] && echo "MOBILIZON_SPAM_EXPORT_FOLDER (${MOBILIZON_SPAM_EXPORT_FOLDER}) does not exist, creating..." && mkdir ${MOBILIZON_SPAM_EXPORT_FOLDER}
[ ! -w ${MOBILIZON_SPAM_EXPORT_FOLDER} ] && echo "MOBILIZON_SPAM_EXPORT_FOLDER (${MOBILIZON_SPAM_EXPORT_FOLDER}) is not writable, please fix permissions" && exit 4
[ ! -f ${MOBILIZON_SPAM_FILE_EMAILS_DOMAINS} ] && echo "MOBILIZON_SPAM_FILE_EMAILS_DOMAINS (${MOBILIZON_SPAM_FILE_EMAILS_DOMAINS}) is missing" && exit 5
[ ! -r ${MOBILIZON_SPAM_FILE_EMAILS_DOMAINS} ] && echo "MOBILIZON_SPAM_FILE_EMAILS_DOMAINS (${MOBILIZON_SPAM_FILE_EMAILS_DOMAINS}) is not readable" && exit 6
[ ! -f ${MOBILIZON_QUERY_TPL_KEYWORDS} ] && echo "MOBILIZON_QUERY_TPL_KEYWORDS (${MOBILIZON_QUERY_TPL_KEYWORDS}) is missing" && exit 7
[ ! -r ${MOBILIZON_QUERY_TPL_KEYWORDS} ] && echo "MOBILIZON_QUERY_TPL_KEYWORDS (${MOBILIZON_QUERY_TPL_KEYWORDS}) is not readable" && exit 8
[ ! -f ${MOBILIZON_QUERY_TPL_EMAILS_DOMAINS} ] && echo "MOBILIZON_QUERY_TPL_EMAILS_DOMAINS (${MOBILIZON_QUERY_TPL_EMAILS_DOMAINS}) is missing" && exit 9
[ ! -r ${MOBILIZON_QUERY_TPL_EMAILS_DOMAINS} ] && echo "MOBILIZON_QUERY_TPL_EMAILS_DOMAINS (${MOBILIZON_QUERY_TPL_EMAILS_DOMAINS}) is not readable" && exit 10

# 0. get last_check datetime if present
#######################################
RUNTIME_SCRIPT=$(date "+%Y-%m-%d %H:%M:%S%:::z")
MOBILIZON_SPAM_LAST_CHECK_STRING="1970-01-01 00:00:00+01"
if [ -f ${MOBILIZON_SPAM_LAST_CHECK} ]
then
    PATTERN='[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{2}'
    grep -E "${PATTERN}" ${MOBILIZON_SPAM_LAST_CHECK}
    if [ $? -eq 0 ]
    then
        MOBILIZON_SPAM_LAST_CHECK_STRING=$(grep -E "${PATTERN}" ${MOBILIZON_SPAM_LAST_CHECK})
    fi
fi

# 1. cleanup last CSV files if any
###################################
rm -v ${MOBILIZON_SPAM_EXPORT_FOLDER}/*.csv

# 2. query the database (filter=keyword) and export results in CSV files
########################################################################
for keyword_spam in $( < ${MOBILIZON_SPAM_FILE_KEYWORDS})
do
	echo == $keyword_spam ==
	sed -e "s/__PATTERN__/${keyword_spam}/g" -e "s/__MOBILIZON_SPAM_LAST_CHECK__/${MOBILIZON_SPAM_LAST_CHECK_STRING}/g" ${MOBILIZON_QUERY_TPL_KEYWORDS} | # replace __PATTERN__ by keyword in our template SQL query
    PGPASSWORD=${MOBILIZON_DATABASE_PASSWORD} psql -h ${MOBILIZON_DATABASE_HOST} -U ${MOBILIZON_DATABASE_USERNAME} -d ${MOBILIZON_DATABASE_DBNAME} -t -A -F"," > ${MOBILIZON_SPAM_EXPORT_FOLDER}/${keyword_spam}.csv  # export result in csv file

	# 2.1 call graphQL API
	while IFS="," read -r event_id user_id url
	do
gql='mutation CreateReport($eventsIds: [ID], $reportedId: ID!, $content: String, $commentsIds: [ID], $forward: Boolean) {
    createReport(
        eventsIds: $eventsIds
        reportedId: $reportedId
        content: $content
        commentsIds: $commentsIds
        forward: $forward
    ) {
        id
        __typename
    }
}'

		printf -v vars '{ "content" : "Possibly SPAM for keyword **%s**, detected by script on %s", "reportedId": "%s", "eventsIds": ["%s"], "forward":false }' "$keyword_spam" "$RUNTIME_SCRIPT" $user_id $event_id

		curl -fsS \
            -H 'accept: application/json' \
            -X 'POST' \
            -H "Authorization: Bearer ${BEARER}" \
            -F "query=${gql}" \
            -F "variables=${vars}" \
            -F "operationName=CreateReport" \
            "${MOBILIZON_EP}"

	  done < "${MOBILIZON_SPAM_EXPORT_FOLDER}/${keyword_spam}.csv"
done

# 3. query the database to get emails of organizers
####################################################
echo "== Getting organizers from events since last check =="

sed -e "s/__MOBILIZON_SPAM_LAST_CHECK__/${MOBILIZON_SPAM_LAST_CHECK_STRING}/g" ${MOBILIZON_QUERY_TPL_EMAILS_DOMAINS} |
PGPASSWORD=${MOBILIZON_DATABASE_PASSWORD} psql -h ${MOBILIZON_DATABASE_HOST} -U ${MOBILIZON_DATABASE_USERNAME} -d ${MOBILIZON_DATABASE_DBNAME} -t -A -F"," > ${MOBILIZON_SPAM_EXPORT_FOLDER}/all_events_with_organizers_emails.csv  # export result in csv file



# add @ at the beginning and copy to temp folder
sed 's/^/@/g' ${MOBILIZON_SPAM_FILE_EMAILS_DOMAINS} > ${MOBILIZON_SPAM_EXPORT_FOLDER}/domains_to_check.csv

# filter
grep -i -f ${MOBILIZON_SPAM_EXPORT_FOLDER}/domains_to_check.csv ${MOBILIZON_SPAM_EXPORT_FOLDER}/all_events_with_organizers_emails.csv > ${MOBILIZON_SPAM_EXPORT_FOLDER}/events_users_with_bad_emails.csv


# 3.1 call graphQL API
while IFS="," read -r event_id user_id email
do
gql='mutation CreateReport($eventsIds: [ID], $reportedId: ID!, $content: String, $commentsIds: [ID], $forward: Boolean) {
createReport(
    eventsIds: $eventsIds
    reportedId: $reportedId
    content: $content
    commentsIds: $commentsIds
    forward: $forward
) {
    id
    __typename
}
}'

    printf -v vars '{ "content" : "Possibly SPAM, user email **%s** from suspicious domain, detected by script on %s", "reportedId": "%s", "eventsIds": ["%s"], "forward":false }' "$email" "$RUNTIME_SCRIPT" $user_id $event_id

    curl -fsS \
        -H 'accept: application/json' \
        -X 'POST' \
        -H "Authorization: Bearer ${BEARER}" \
        -F "query=${gql}" \
        -F "variables=${vars}" \
        -F "operationName=CreateReport" \
        "${MOBILIZON_EP}"

done < "${MOBILIZON_SPAM_EXPORT_FOLDER}/events_users_with_bad_emails.csv"


# update MOBILIZON_SPAM_LAST_CHECK
echo $RUNTIME_SCRIPT > ${MOBILIZON_SPAM_LAST_CHECK}
