# Mobilizon SPAM detector

## Contexte

- Le but de ce projet est d'avoir un script exécutable en cron qui, à partir d'une liste de mots-clés, fait un signalement via l'API de Mobilizon à propos des événements qui semblent être du SPAM.

## Fonctionnement

- Il y a 6 fichiers principaux:
    1. ``keywords_spam.txt`` qui est une liste de mots-clés que l'on va chercher dans la base de données. Le séparateur est un retour à la ligne.
    2. ``query.tpl.sql`` contient la requête SQL que l'on exécute pour chaque mot-clé. Le mot-clé est inséré dynamiquement via ``sed``.
    3. ``report_spam.sh`` qui est le script principal en bash et possède deux actions principales:
        1. Lire le fichier des mots-clés et pour chaque entrée, exécuter la requête SQL et stocker le résultat dans un fichier CSV.
        2. À partir de chaque fichier CSV généré, appeler l'API Mobilizon pour faire le signalement.
    4. ``last_check.txt`` contient la date au format ``date "+%Y-%m-%d %H:%M:%S%:::z"`` du dernier check et sinon on met ``1970-01-01 00:00:00+01``
    5. ``get_token.sh`` qui à partir de credentials utilisateur obtient un accessToken nécessaire au report
    6. ``main.sh`` qui permet d'utiliser tous les scripts dans le bon ordre (avoir un token valide puis faire le report)
- Les variables d'environnement utiles pour le projet:


| Nom                          | Description                                                                                     | Valeur par défaut  |
|------------------------------|-------------------------------------------------------------------------------------------------|--------------------|
| MOBILIZON_API_USER_EMAIL     | Adresse email du compte dont on se sert pour s'authentifier à l'API                             |                    |
| MOBILIZON_API_USER_PASSWORD  | Mot de passe du compte dont on se sert pour s'authentifier à l'API                              |                    |
| MOBILIZON_DATABASE_DBNAME    | Nom de la BDD de Mobilizon                                                                      |                    |
| MOBILIZON_DATABASE_HOST      | Nom de l'hôte de la BDD                                                                         |                    |
| MOBILIZON_DATABASE_PASSWORD  | Mot de passe pour se connecter à la BDD                                                         |                    |
| MOBILIZON_DATABASE_USERNAME  | Utilisateur pour se connecter à la BDD                                                          |                    |
| MOBILIZON_EP                 | URL de l'API de Mobilizon                                                                       |                    |
| MOBILIZON_SPAM_EXPORT_FOLDER | Dossier où on exporte les CSV avec les données des reports                                      | spam_export        |
| MOBILIZON_SPAM_FILE_KEYWORDS | Fichier source des mots-clés à chercher dans la Base De Données Mobilizon pour détecter le SPAM | keywords_spam.txt  |
| MOBILIZON_SPAM_LAST_CHECK    | Fichier mémoire où on stocke la date de la dernière exécution du script                         | last_check.txt     |
